import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import 'materialize-css'; // It installs the JS asset only
import './index.scss';

let Latex = require('react-latex');

class Assertion extends React.Component {
  render() {
    return (
        <div className={"card " + this.props.type.color}>
          <div className="card-content">
            <div className="right"><b>{this.props.type.text}</b></div>
            <span className="card-title"><b>{this.props.title}</b></span>
            {this.props.body}<br/>
            {this.props.tags.map(tag => <div key={tag} className="chip grey darken-3 white-text text-darken-3">{tag}</div>)}
          </div>
        </div>
    );
  }
}

class AssertionList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialList: props.list,
      list: props.list,
    }

    this.filterList = this.filterList.bind(this);
  }



  filterList(e) {
    var updatedList = this.state.initialList;
    var search = [];
    var term;

    if (e.target.value.includes(',')) {
      search = e.target.value.split(',');
    } else {
      search.push(e.target.value);
    }
    for (term of search) {
      updatedList = updatedList.filter((assertion) => getSearchableText(assertion).search(term.toLowerCase().replace(/([$.+()\-\\])/g, '\\$1')) !== -1);
    }
    this.setState({ list: updatedList });
  }

  render() {
    return (
      <div id="main">
        <div className="row">
          <div className="col s12">
            <input type="text"
              id="searchbar"
              className="white-text"
              placeholder="Búsqueda"
              onChange={this.filterList}
            />
          </div>
        </div>
        {this.state.list.map(entry => <Assertion key={entry.title} title={entry.title} body={entry.body} type={entry.type} tags={entry.tags} />)}
      </div>
    );
  }

  componentDidMount() {
    document.getElementById("searchbar").focus();
  }
}

function getSearchableText(object) {
  let searchableText = (object.title + " " + ReactDOMServer.renderToStaticMarkup(object.body) + " " + object.tags.join(" ") + " " + object.type.text).toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/<anno[^>]*>[^>]*>/gm, '').replace(/<[^>]*>?/gm, '');
  return searchableText;
}

const assertionType = {
  DEFINITION: {text: "DEFINICIÓN", color: "cyan lighten-3"},
  THEOREM: {text: "TEOREMA", color: "green lighten-3"},
  COROLARY: {text: "COROLARIO", color: "lime lighten-3"},
};

const assertionList = [
  {type: assertionType.DEFINITION, title:"Producto escalar", 
    body: (
      <div>
        <p>
          <Latex>
            {"Sea $\\mathcal{V}$ un espacio vectorial definido sobre el cuerpo $\\mathbb{R}$. Un producto escalar es una función $\\varphi : \\mathcal{V} \\times \\mathcal{V} \\rightarrow \\mathbb{R}$ que verifica:"}
          </Latex>
        </p>
          <ol>
            <li><Latex>{"$\\varphi(\\vec{u},\\vec{v})=\\varphi(\\vec{v},\\vec{u})$"}</Latex></li>
            <li><Latex>{"$\\varphi(\\vec{u},\\vec{v}+\\vec{w})=\\varphi(\\vec{u},\\vec{v})+\\varphi(\\vec{u},\\vec{w})$"}</Latex></li>
            <li><Latex>{"$\\varphi(c\\vec{u},\\vec{v})=\\varphi(\\vec{u},c\\vec{v})=c\\varphi(\\vec{u},\\vec{v})$"}</Latex></li>
            <li><Latex>{"$\\varphi(\\vec{v},\\vec{v})>0$ y $\\varphi(\\vec{v},\\vec{v})=0$ si, y sólo si, $\\vec{v}=\\vec{0}$"}</Latex></li>
          </ol>
      </div>
    ),
    tags: ["tema 8", "espacios euclídeos"]},
  {type: assertionType.DEFINITION, title:"Espacio vectorial euclídeo",
    body: <div><p>{"Se llama espacio euclídeo a un espacio vectorial real en el que se ha definido un producto escalar"}</p></div>,
    tags: ["tema 8", "espacios euclídeos"]},
  {type: assertionType.DEFINITION, title:"Norma de un vector",
    body: <div><p><Latex>{"Sea $(\\mathcal{V},\\langle\\rangle)$ un espacio vectorial euclídeo. Llamamos norma de un vector $\\vec{v} \\in \\mathcal{V}$ al número real"}</Latex><Latex displayMode={true}>{"$$\\|\\vec{v}\\|=\\sqrt{\\langle\\vec{v},\\vec{v}\\rangle}$$"}</Latex></p></div>,
    tags: ["tema 8", "espacios euclídeos"]},
  {type: assertionType.DEFINITION, title:"Vector unitario",
    body: <div><p><Latex>{"Sea $(\\mathcal{V},\\langle\\rangle)$ un espacio vectorial euclídeo. Se dice que $\\vec{u} \\in \\mathcal{V}$ es un vector unitario si $\\|\\vec{u}\\|=1$"}</Latex></p><p><Latex>{"Si $\\vec{v}$ es un vector no nulo, entonces el vector $\\vec{u}=\\frac{1}{\\|\\vec{v}\\|}\\vec{v}$ es unitario"}</Latex></p></div>,
    tags: ["tema 8", "espacios euclídeos"]},
  {type: assertionType.DEFINITION, title:"Distancia entre dos vectores",
    body: <div><p><Latex>{"Sea $(\\mathcal{V},\\langle\\rangle)$ un espacio vectorial euclídeo. Llamamos distancia entre dos vectores $\\vec{v},\\vec{w} \\in \\ \\mathcal{V}$ al número real"}</Latex><Latex displayMode={true}>{"$$d(\\vec{v},\\vec{w})=\\|\\vec{v}-\\vec{w}\\|$$"}</Latex></p></div>,
    tags: ["tema 8", "espacios euclídeos"]},
  {type: assertionType.THEOREM, title: "Propiedades de la norma",
    body: (
      <div><p><Latex>{"Sea $(\\mathcal{V},\\langle\\rangle)$ un espacio vectorial euclídeo y sean $c \\in \\mathbb{R}, \\vec{v}, \\vec{w} \\in \\mathcal{V}$."}</Latex></p>
        <ol>
          <li><Latex>{"$\\|\\vec{v}\\|=0$ si, y sólo si, $\\vec{v}=0$"}</Latex></li>
          <li><Latex>{"$\\|c\\vec{v}\\|=|c|$ $\\|v\\|$"}</Latex></li>
          <li><Latex>{"$\\|\\vec{v}+\\vec{w}\\|\\leq\\|v\\|+\\|w\\|$"}</Latex> <i>(Desigualdad triangular)</i></li>
        </ol>
      </div>
    ),
    tags: ["tema 8", "espacios euclídeos"]},
  {type: assertionType.THEOREM, title: "Propiedades de la distancia",
    body: (
      <div><p><Latex>{"Sean $\\vec{v},\\vec{w}$ vectores de un espacio euclídeo $(\\mathcal{V},\\langle\\rangle)$. Entonces"}</Latex></p>
        <ol>
          <li><Latex>{"$d(\\vec{v},\\vec{w}) \\geq 0$"}</Latex></li>
          <li><Latex>{"$d(\\vec{v},\\vec{w})=0$ si, y sólo si, $\\vec{v}=\\vec{w}$"}</Latex></li>
          <li><Latex>{"$d(\\vec{v},\\vec{w})=d(\\vec{w},\\vec{v})$"}</Latex></li>
          <li><Latex>{"$d(\\vec{v},\\vec{w}) \\leq d(\\vec{v},\\vec{u})+d(\\vec{u},\\vec{w})$"}</Latex> <i>(Desigualdad triangular)</i></li>
        </ol>
      </div>
    ),
    tags: ["tema 8", "espacios euclídeos"]},

];


ReactDOM.render(<AssertionList list={assertionList}/>, document.getElementById("root"));
